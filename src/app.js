'use strict';



/**
 * Declaration de l'app
 */
angular.module('deimos-dashboard', [
    'ngRoute',
    'ngSanitize',
    'ngResource',
    'angularMoment',
    'xeditable',
    'angularFileUpload',
    'angularSpinner',
    '19degrees.ngSweetAlert2',
    'ui.bootstrap', 'ui.bootstrap.datetimepicker',
    'textAngular',
    'ui-notification',
]);

/**
 * Configuration de $http
 */
angular
    .module('deimos-dashboard')
        .config(['$httpProvider', '$sceProvider', ($httpProvider, $sceProvider) => {

            $httpProvider.defaults.useXDomain = true;
            $httpProvider.defaults.withCredentials = false;
            $sceProvider.enabled(false);
        }]);

angular.module('deimos-dashboard').config(function() {
    angular.lowercase = angular.$$lowercase;  
    angular.uppercase = angular.$$uppercase;  
});

/**
 * Configuration de moment.js
 */
angular
    .module('deimos-dashboard')
        .run(['amMoment', 'editableOptions', function (amMoment, editableOptions){
            amMoment.changeLocale('fr');
            editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
        }])

