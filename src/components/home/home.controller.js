angular
    .module('deimos-dashboard')
        .controller('homeController', homeController);

homeController.$inject = ['$rootScope', '$location', '$window', '$document', '$interval', 'sweetAlert']
function homeController($rootScope, $location, $window, $document, $interval, sweetAlert) {

    var vm = this;
    vm.viewLoaded = false
    vm.manualMode = false


    activate();
    async function activate(){

        /**
         * Gauge direction
         */
        var direction_gauge_opts  = {
            angle: 0, // The span of the gauge arc
            lineWidth: 0.1, // The line thickness
            radiusScale: 0.5, // Relative radius
            pointer: {
                length: 0.6, // // Relative to gauge radius
                strokeWidth: 0.02, // The thickness
                color: '#02FEFF' // Fill color
            },
            limitMax: false,     // If false, max value increases automatically if value > maxValue
            limitMin: false,     // If true, the min value of the gauge will be fixed
            colorStart: '#E0E0E0',   // Colors
            colorStop: '#E0E0E0',    // just experiment with them
            strokeColor: '#E0E0E0',  // to see which ones work best for you
            generateGradient: true,
            highDpiSupport: true,     // High resolution support
            renderTicks: {
                divisions: 5,
                divWidth: 1.1,
                divLength: 0.7,
                divColor: '#333333',
                subDivisions: 3,
                subLength: 0.5,
                subWidth: 1,
                subColor: '#666666'
            },
            staticLabels: {
                font: "10px sans-serif",  
                labels: [0, 90, 180],
                color: "#02FEFF",  
            },
            staticZones: [
                {strokeStyle: "#F03E3E", min: 89, max: 91}, // Red center
                {strokeStyle: "#F03E3E", min: 0, max: 2}, // Red min
                {strokeStyle: "#F03E3E", min: 178, max: 180}, // Red max
            ]
        };

        let direction_target = angular.element( document.querySelector( '#home_gauge_direction' ) )[0]; // your canvas element
        vm.direction_gauge = new Gauge(direction_target).setOptions(direction_gauge_opts); // create sexy gauge!
        vm.direction_gauge.maxValue = 180; // set max gauge value
        vm.direction_gauge.setMinValue(90);  // Prefer setter over gauge.minValue = 0
        vm.direction_gauge.animationSpeed = 40; // set animation speed (32 is default value)
        vm.direction_gauge.set(90); // set actual value

        $rootScope.$watch('realtime_direction', () => {

            if(!$rootScope.manualMode){
                let bracage = parseInt($rootScope.realtime_direction) - 270
                let angle = bracage/1.3
                vm.direction_gauge.set(angle)
            } else {
                vm.direction_gauge.set($rootScope.realtime_direction)
            }
            
        })

        // $scope.$on('$destroy', function() {
        // });

        /**
         * Gauge speed
         */
        var speed_gauge_opts  = {
            angle: 0, // The span of the gauge arc
            lineWidth: 0.18, // The line thickness
            radiusScale: 0.73, // Relative radius
            pointer: {
                length: 0.43, // // Relative to gauge radius
                strokeWidth: 0.015, // The thickness
                color: '#02FEFF' // Fill color
            },
            limitMax: true,     // If false, max value increases automatically if value > maxValue
            limitMin: true,     // If true, the min value of the gauge will be fixed
            colorStart: '#E0E0E0',   // Colors
            colorStop: '#E0E0E0',    // just experiment with them
            strokeColor: '#E0E0E0',  // to see which ones work best for you
            generateGradient: true,
            highDpiSupport: true,     // High resolution support
            // renderTicks is Optional
            renderTicks: {
                divisions: 5,
                divWidth: 1.1,
                divLength: 0.7,
                divColor: '#333333',
                subDivisions: 3,
                subLength: 0.5,
                subWidth: 1,
                subColor: '#666666'
            },
            staticLabels: {
                font: "10px sans-serif",  
                labels: [-140, -120, -100, -80, -60, -40, -20, 0, 20, 40, 60, 80, 100, 120, 140],
                color: "#02FEFF",  
            },
            staticZones: [
                {strokeStyle: "#F03E3E", min: -1, max: 1},
                {strokeStyle: "#F03E3E", min: 138, max: 140}, // Red max
                {strokeStyle: "#F03E3E", min: -140, max: -138}, // Red min
            ]
        };

        let speed_target = angular.element( document.querySelector( '#home_gauge_speed' ) )[0]; // your canvas element
        vm.speed_gauge = new Gauge(speed_target).setOptions(speed_gauge_opts); // create sexy gauge!
        vm.speed_gauge.maxValue = 140; // set max gauge value
        vm.speed_gauge.setMinValue(-140);  // Prefer setter over gauge.minValue = 0
        vm.speed_gauge.animationSpeed = 40; // set animation speed (32 is default value)
        vm.speed_gauge.set(0); // set actual value

        $rootScope.$watch('realtime_speed', () => {
            let speed
            if(!$rootScope.manualMode)
                speed = parseInt($rootScope.realtime_speed) - 360
            else
                speed = $rootScope.realtime_speed
            vm.speed_gauge.set(speed)
        })
        
    }


}
