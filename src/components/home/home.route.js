angular
    .module('deimos-dashboard')
        .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){

    $routeProvider
        
        //Teams routes
        .when('/', {
            templateUrl: '/src/components/home/home.html',
            controller: 'homeController as vm',
            controllerAs: "vm",
        })
}]);