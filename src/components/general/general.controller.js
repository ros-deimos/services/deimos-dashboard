angular
    .module('deimos-dashboard')
        .controller('generalController', generalController);

generalController.$inject = ['$rootScope', '$window', '$location', '$route', '$interval', "$http", '__env', 'moment'];
function generalController($rootScope, $window, $location, $route, $interval, $http, __env, moment) {
    console.log('generalController  loaded')
    var vm = this;
    vm.env = __env;
    vm.background_src = "/assets/images/load.gif"

    $interval(() => {
        vm.hour = moment().format('HH:mm');
        vm.day = moment().format('dddd');
        vm.day_nb = moment().format('DD');
        vm.month = moment().format('MMMM');
    }, 1000);
    

    activate();
    async function activate(){
        console.log('General controller activated')
        /**
         * Init variables
         */
        $rootScope.sockets = {};    
        $rootScope.rt_value = {}; 

        $rootScope.browserGamepadCompatible = detectController()
        $rootScope.gamepadConnected = false
        $rootScope.manualMode = false
        $rootScope.debugMode = false
        $rootScope.toggleManualMode = toggleManualMode
        $rootScope.toggleDebugMode = toggleDebugMode

        /**
         * Connect sockets
         */

        let socketOpts = {
            transports: ['websocket'], 
            upgrade: false
        }

        /**
         * Bootstrap video sockets interactions
         */
        $rootScope.sockets.video = io(__env.WEBSOCKET_CAM_ENDPOINT, socketOpts)
        $rootScope.sockets.video.on('connect', () => {
            console.log('Video socket connected...')
            $rootScope.sockets.video.on('ros:data', (msg) => {
                if(!$rootScope.debugMode){
                    $rootScope.realtime_image = `data:image/jpeg;base64,${msg.data}`
                    $rootScope.$apply()
                }
            })
        })


        /**
         * Debug video sockets interactions
         */
        $rootScope.sockets.debug = io(__env.WEBSOCKET_DEBUG_ENDPOINT, socketOpts)
        $rootScope.sockets.debug.on('connect', () => {
            console.log('Debug socket connected...')
            $rootScope.sockets.debug.on('ros:data', (msg) => {
                if($rootScope.debugMode){
                    $rootScope.realtime_image = `data:image/jpeg;base64,${msg.data}`
                    $rootScope.$apply()
                }
            })
        })

        /**
         * Bootstrap direction sockets interactions
         */
        $rootScope.sockets.direction = new WebSocket(__env.WEBSOCKET_DIRECTION_ENDPOINT);
        $rootScope.sockets.direction.onopen = function() {
            console.log('Direction socket connected...')
        }

        $rootScope.sockets.direction.onmessage = function(msg) {
            if(!$rootScope.manualMode)
                $rootScope.realtime_direction = JSON.parse(msg.data).data

            if(__env.DEBUG)
                console.log("message: ", msg)
        }

        /**
         * Bootstrap speed sockets interactions
         */
        $rootScope.sockets.speed = new WebSocket(__env.WEBSOCKET_SPEED_ENDPOINT);
        $rootScope.sockets.speed.onopen = function() {
            console.log('Speed socket connected...')
        }

        $rootScope.sockets.speed.onmessage = function (msg) {
            if(!$rootScope.manualMode)
                $rootScope.realtime_speed = JSON.parse(msg.data).data

            if(__env.DEBUG)
                console.log("message: ", msg)
        }


        /**
         * Manual mode init
         */
        if($rootScope.browserGamepadCompatible) {

            $window.addEventListener("gamepadconnected", function(e) {
                console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
                                e.gamepad.index, e.gamepad.id,
                                e.gamepad.buttons.length, e.gamepad.axes.length
                            );

                $rootScope.gamepadConnected = true
            });

            $window.addEventListener("gamepaddisconnected", function(e) {
                console.log("Gamepad disconnected from index %d: %s", e.gamepad.index, e.gamepad.id);
                $rootScope.gamepadConnected = false
            });
        }
    }

    /**
     * Manual mode part
     */
    async function toggleManualMode() {

        if(!$rootScope.gamepadConnected)
            return Promise.reject()

        /**
         * Activate manual mode
         */
        if(!$rootScope.manualMode){
            console.log('Manual mode enable !')
            $rootScope.manualMode = true
            let control_request = await $http.get(`http://${__env.CAR_CONTROL_ENDPOINT}/manual?manual=true`);
            alert(`Manual control request response: ${control_request}`)
            $rootScope.gamepad = new XBoxController()
            $rootScope.gamepad.init()
            $rootScope.gamepad.on('gamepad:refresh', (status) => {
                console.log(status)
                $rootScope.realtime_direction = status.direction
                $rootScope.realtime_speed = status.speed

                /**
                 * Process data before send it to the deimos car
                 */
                let dir = status.direction*1.3
                dir += 270
                
                $rootScope.sockets.direction.send(dir)
                $rootScope.sockets.speed.send(status.speed+360)
            })


        } else
        /**
         * Desactivate manual mode
         */
        {
            console.log('Manual mode disable !')
            $rootScope.manualMode = false
            let control_request = await $http.get(`http://${__env.CAR_CONTROL_ENDPOINT}/manual?manual=false`);
            alert(`Manual control request response: ${control_request}`)
            $rootScope.gamepad.disconnect()
            $rootScope.gamepad = undefined
            
        }
    }
    /**
     * Debug mode part
     */
    async function toggleDebugMode() {
        if(!$rootScope.debugMode){
            console.log('Debug mode enable !')
            $rootScope.debugMode = true 
        } else{
            console.log('Debug mode disable !')
            $rootScope.debugMode = false            
        }
    }

    function detectController() {
        return "getGamepads" in navigator
    }
}