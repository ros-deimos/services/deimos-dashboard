angular
    .module('deimos-dashboard')
        .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){

    $routeProvider

        .otherwise({
            redirectTo: '/'
        });
}]);