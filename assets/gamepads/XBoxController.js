
class XBoxController extends EventEmitter3 {
    
    init() {

        this.state = {
            speed: 0,
            direction: 90
        }

        let that = this

        console.log(this)

        this.emitState = new EventTarget()
        this.refreshInterfal = setInterval(() => {
            that.gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
            that.gamepad = that.gamepads[0]
            /**
             * Get gamepad values
             * Check: https://html5gamepad.com/
             */
            let direction = 90 + parseInt(that.gamepad.axes[0] * -90)


            let speed
            if(that.gamepad.buttons[7].value !== 0)
                if(that.gamepad.buttons[7].value > 0)
                    speed = parseInt(that.gamepad.buttons[7].value * 140)
                else
                    speed = parseInt(that.gamepad.buttons[6].value * -140)
            else if(that.gamepad.buttons[6].value > 0)
                speed = parseInt(that.gamepad.buttons[6].value * -140)
            else
                speed = 0
            
            if(speed !== that.state.speed || direction !== that.state.direction){
                console.log("Detect gamepad change ")
                that.state = {
                    direction: that.filterAndProcessDirection(direction),
                    speed: that.filterAndProcessSpeed(speed)
                }
                that.emit('gamepad:refresh', that.state);
            }            
        }, 20)
        this.emit('gamepad:refresh', this.state);
    }

    getState() {
        return this.state
    }

        
    /**
     * Filter change of speed
     * Min: 0
     * Max: 180
     * Center: 90
     */
    filterAndProcessDirection(input) {
        /**
         * If turn right
         */
        if(input >= this.direction) {
            /**
             * Set max rotate from previous refresh as 15
             */
            if(input > this.direction + 15) {
               return this.direction + 15
            } else {
               return input
            }
        /**
         * If turn left ( input < this.direction )
         */
        } else {
            /**
             * Set max rotate from previous refresh as 15
             */
            if(input < this.direction - 15) {
                return this.direction - 15
            } else {
                return input
            }
        }
    }

    /**
     * Filter change of speed
     * Min: -140
     * Max: 140
     * Center: 0
     */
    filterAndProcessSpeed(input) {
        /**
         * If accelerate
         */
        if(input > this.speed) {
            /**
             * Set max rotate from previous refresh as 10
             */
            if(input > this.speed + 10) {
                return this.speed + 10
            } else {
                return input
            }
        /**
         * If decelerate ( input < this.speed )
         */
        } else {
            /**
             * Set max rotate from previous refresh as 10
             */
            if(input < this.speed - 10 ) {
                return this.speed - 10
            } else {
                return input
            }
        }
    }

    disconnect() {
        clearInterval(this.refreshInterfal)
        this.gamepads = []
        this.gamepad = undefined
        this.state = undefined
    }
}