#!/bin/bash
echo "Configuring UI to use websockets "
echo "Camera endpoint: $WEBSOCKET_CAM_ENDPOINT"
echo "Debug video endpoint: $WEBSOCKET_DEBUG_ENDPOINT"
echo "Direction endpoint: $WEBSOCKET_DIRECTION_ENDPOINT"
echo "Speed endpoint: $WEBSOCKET_SPEED_ENDPOINT"

sed -i "s#{{WEBSOCKET_CAM_ENDPOINT}}#$WEBSOCKET_CAM_ENDPOINT#g" /var/www/dist/app.js
sed -i "s#{{WEBSOCKET_DEBUG_ENDPOINT}}#$WEBSOCKET_DEBUG_ENDPOINT#g" /var/www/dist/app.js
sed -i "s#{{WEBSOCKET_DIRECTION_ENDPOINT}}#$WEBSOCKET_DIRECTION_ENDPOINT#g" /var/www/dist/app.js
sed -i "s#{{WEBSOCKET_SPEED_ENDPOINT}}#$WEBSOCKET_SPEED_ENDPOINT#g" /var/www/dist/app.js
sed -i "s#{{CAR_CONTROL_ENDPOINT}}#$CAR_CONTROL_ENDPOINT#g" /var/www/dist/app.js

echo "Starting web server"
chgrp -R www-data /var/www
chmod -R g+rw /var/www
/usr/sbin/apache2ctl -D FOREGROUND
