# Deimos dashboard

It is a dashboard for the Deimos project's car.This web application is an AngularJS application connected to socket.io websocket. This websockets are provided by [ROS Websocket Gateway](https://gitlab.com/ros-deimos/services/ros-websocket-gateway) services. 

![Preview](assets/images/preview.png)


## Features

* Live mode: display real time video, speed, and direction of Deimos car
* Manual mode: controll the Deimos car with gamepad from the browser (only XBox controller is supported now)


## TODO

* Metrics page
* Configs page
* Debug mode: specific view for the debug
* Display real time `ROS` Master informations


## Usage and Development
You can find this informations in some git repositories of the Deimos project
* For usage with the Deimos car, please check this [group of git repositories](https://gitlab.com/ros-deimos/docker-compose-architectures/with-hardware). In this, you can find docker-compose stacks of the car's stack and the dashboard stack.

* For development, please check this [git repositorie](https://gitlab.com/ros-deimos/docker-compose-architectures/dev/deimos-dashboard). In this, you can find docker-compose stacks of the dashboard stack with integrated `ROS` publishers of speed, direction and image recorded data.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)