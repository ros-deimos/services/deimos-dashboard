const gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    gutil = require('gulp-util'),
    CacheBuster = require('gulp-cachebust');

const cachebust = new CacheBuster();

/////////////////////////////////////////////////////////////////////////////////////
//
// cleans the build output
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('clean', function (cb) {
    del([
        'dist'
    ], cb);
});

/////////////////////////////////////////////////////////////////////////////////////
//
// runs "npm install" to install frontend dependencies
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('install', function() {

    var install = require("gulp-install");
    return gulp.src(['./package.json'])
            .pipe(install());
});


/////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////////////
//
// Build a minified Javascript bundle - the order of the js files is determined
// by browserify
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('bundle', [],function() {
    gulp.src([
        './node_modules/eventemitter3/umd/eventemitter3.js',
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
        './node_modules/sweetalert2/dist/sweetalert2.js',
        './node_modules/moment/moment.js',
        './node_modules/moment/locale/fr.js',
        './node_modules/socket.io-client/dist/socket.io.js',
        './node_modules/angular/angular.js',
        './node_modules/angular-route/angular-route.min.js',
        './node_modules/angular-sanitize/angular-sanitize.min.js',
        './node_modules/angular-resource/angular-resource.min.js',
        './node_modules/angular-aria/angular-aria.min.js',
        './node_modules/angular-animate/angular-animate.min.js',
        './node_modules/angular-messages/angular-messages.min.js',
        './node_modules/angular-material/angular-material.min.js',
        './node_modules/angular-xeditable/dist/js/xeditable.js',
        './node_modules/angular-touch/angular-touch.min.js',
        './node_modules/angular-ui-notification/dist/angular-ui-notification.min.js',
        './node_modules/angular-moment/angular-moment.min.js',
        './node_modules/angular-spinner/dist/angular-spinner.min.js',
        './node_modules/angular-smart-table/dist/smart-table.js',
        './node_modules/angular-datetime-range/dist/datetime-range.min.js',
        './node_modules/angular-file-upload/dist/angular-file-upload.min.js',
        './node_modules/angular-recaptcha/release/angular-recaptcha.min.js',
        './node_modules/textangular/dist/textAngular-rangy.min.js',
        './node_modules/textangular/dist/textAngular-sanitize.min.js',
        './node_modules/textangular/dist/textAngularSetup.js',
        './node_modules/textangular/dist/textAngular.min.js',
        './node_modules/jquery-bracket/dist/jquery.bracket.min.js',
        './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        './node_modules/bootstrap-ui-datetime-picker/dist/datetime-picker.js',
        './node_modules/ng-table/bundles/ng-table.min.js',
        './assets/angularjs/ngSweetAlert2/SweetAlert.js',
        './assets/angularjs/gaugejs/gauge.min.js',
        './assets/gamepads/XBoxController.js',
        './src/app.js',
        './src/env.js',
        './src/components/**/*.js',
        './src/default-routes.js'
    ])
    .pipe(concat('app.js'))
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(gulp.dest('./dist/'))
});

/////////////////////////////////////////////////////////////////////////////////////
//
// full build (except sprites), applies cache busting to the main page css and js bundles
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build', [ 'clean', 'bundle'], function() {});

/////////////////////////////////////////////////////////////////////////////////////
//
// watches file system and triggers a build when a modification is detected
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('watch', function() {
    return gulp.watch(['./index.html','./assets/css/*.css', './src/**/*.js', './src/**/*.html'], ['build']);
});

/////////////////////////////////////////////////////////////////////////////////////
//
// launches a web server that serves files in the current directory
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('webserver', ['watch','bundle'], function() {
    gulp.src('.')
        .pipe(webserver({
            livereload: true,
            fallback: './index.html',
            directoryListing: false,
            open: "http://localhost:8000/"
        }));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// launch a build upon modification and publish it to a running server
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('dev', ['watch', 'webserver']);



/////////////////////////////////////////////////////////////////////////////////////
//
// installs and builds everything, including sprites
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('default', ['build']);