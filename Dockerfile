FROM node:9-jessie as builder

WORKDIR /usr/src
COPY . .

RUN npm install -g gulp-cli && npm install
RUN gulp bundle

FROM registry.gitlab.com/bliiitz/apache2:stable

ARG CI_COMMIT_SHA=""
ARG CI_COMMIT_TAG=""

COPY --from=builder /usr/src /var/www
ADD vhost.conf /etc/apache2/sites-available/000-default.conf

RUN cd /var/www && \
    cp init_website_env.sh /init_website_env.sh && \
    chmod +x /init_website_env.sh 

RUN sed -i "s/{{CI_COMMIT_SHA}}/$CI_COMMIT_SHA/g" /var/www/index.html
RUN if [ -z "$CI_COMMIT_TAG" ]; then VERSION=$CI_COMMIT_SHA; else VERSION=$CI_COMMIT_TAG; fi && echo $VERSION && sed -i "s/{{VERSION}}/$VERSION/g" /var/www/index.html && sed -i "s/{{VERSION}}/$VERSION/g" /var/www/dist/app.js

EXPOSE 80
CMD ["/init_website_env.sh"]
